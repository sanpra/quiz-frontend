export interface Register {
  data: {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    confirmPassword: string;
  };
  message?: string;
  status?: number;
}

export interface Login {
  data: {
    _id: string,
    email: string,
    password: string,
    role: string,
    logiAttempt: number,
    lastLoggedInAt: number,
    accessToken: string
  };
  message?: string;
  status?: number;
}

export interface Quiz {
  status: number;
  data: {
    quiz:
    {
      _id: string;
      title: string;
      description?: string;
      endDate: Date;
      duration: number;
      createdOn: string;
      createdAt: Date;
      status: string;
      expireDate: Date;
    },
    totalPages: number;
    totalRecords: number;
    result: string;
  };
  message: string;
}

export interface DialogData {
  id?: string;
  title?: string;
  isdata?: boolean;
  msg?: string;
  description?: string;
  duration?: number;
}
export interface ResultData {
  result?: number;
  userScore?: number;
  userPercentage?: number;
  isforResult: boolean;
}

export interface ResultReport {
  quizTitle: string;
  result: {
    _id: string,
    title: string,
    options: [{
      _id: string,
      text: string,
      isCorrect: boolean,
      useSelected: boolean
    }]
  };
}
