export class User {
  data: {
    _id: string;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    confirmPassword: string;
    role?: string;
  };
}
