import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../sharedServices/auth.service';
import { QuizQuestionsComponent } from 'src/app/pages/quiz-questions/quiz-questions.component';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

constructor(private router: Router , private auth: AuthService) { }


canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.auth.isLoggednIn()) {
    return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

  canDeactivate(
    component: QuizQuestionsComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

return component.canExit();
}

}
