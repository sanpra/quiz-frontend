import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sharedComponent/sidebar/sidebar.component';
import { NavbarComponent } from './sharedComponent/navbar/navbar.component';
import { FooterComponent } from './sharedComponent/footer/footer.component';



@NgModule({
  declarations: [SidebarComponent, NavbarComponent, FooterComponent],
  imports: [CommonModule, MaterialModule],
  exports: [SidebarComponent, NavbarComponent, FooterComponent]
})
export class SharedModule { }
