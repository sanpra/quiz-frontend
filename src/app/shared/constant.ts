export const i18N = {
  labels: {
    submitBtn: 'SUBMIT',
    loginBtn: 'LOGIN',
    resetPwd: 'Reset Password',
    registerBtn: 'REGISTER',
    notRegister: 'Don\'t have an account?',
    alreadyRegister: 'If Already Member?',
    forgotBtn: 'Forgot Password?',
    registerNow: 'Register Now!',
    loginNow: 'Login',
    loginLogo: './assets/images/logo1.png',
    toolbarLogo: './assets/images/logo2.png',
    altText: 'RQS-logo.png',
    previousBtn: 'Previous',
    nextBtn: 'Next',
    saveBtn: 'Save',
    doneBtn: 'Done',
    savenextBtn: 'Save & Next',
    quizResult: 'QUIZ RESULT',
    result: 'Result',
    score: 'Score',
    percentage: 'Percentage',
    yesBtn: 'Yes',
    noBtn: 'No',
    minutes: 'Min',
    back: 'Back to home',
    okBtn: 'OK'
  },
  messages: {
    requireField: (fieldName: string) => `${fieldName} is required`,
    invalidField: (fieldName: string) => `${fieldName} is invalid`,
    confirmPassword: (fieldName: string) => `${fieldName} password not matched`,
    registerSuccess: 'Registered successfully',
    loginSuccess: ' LoggedIn successfully',
    logoutSuccess: 'Logout successfully',
    tokenInvalid: 'Time out',
    minCharaters: 'Minimum 8 character',
    alert: 'You have been logged out because your session expired or due to unauthorized access',
    searchMessage: 'Search Quiz',
    notAttempted: 'You have not appeared for the quiz',
    appeard: 'You already appeared Quiz and you are',
    browserBackAlert: 'The quiz will be submitted if you choose to OK',
    submitAlert: 'Are you sure want to submit the Quiz?',
    warningAlert: 'Hurry up! Only 1 Minute Left',
    timeout: 'Time over',
    serverError: 'Something went wrong. please try after some time',
    reloadError: 'Quiz submitted because you choose to reload',
    comingSoon: 'coming soon'
  },
  regex: {
    email: '^[a-z.A-Z_]+@rishabhsoft+\.com$',
    password: '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$'
  },
  placeholder: {
    firstName: 'First Name',
    lastName: 'Last Name',
    email: 'Email',
    password: 'Password',
    confirmPassword: 'Confirm Password'
  },
  status: {
    Ok: 200,
    Unauthorized: 401,
    InternalServerError: 500,
    BadReqquest: 400,
    SessionExpired: 403
  },
  type: {
    text: 'text',
    password: 'password',
    email: 'email',
    radio: 'radio',
    checkbox: 'check box',
  },
  instruction: {
    instro: 'Instructions',
    title: 'Title',
    points: (duration) => [

      { text: 'All questions are mandatory' },
      { text: `This is a timed quiz. you get ${duration} minutes to answer all questions` },
      { text: 'There is only attempt. if you cancel or close quiz mid-way, you will lose your attempt no score will be evaluated.' }
    ],
    description: 'Description'
  },
  quizStatus: {
    created: 'Available',
    expired: 'Expired'
  },
  result: {
    pass: 'Passed',
    fail: 'Failed'
  },
  time: {
    week: 'Week',
    weeks: 'Weeks',
    day: 'Day',
    days: 'Days',
  },
  toaster: {
    action: 'X',
    panel: { success: 'success', warning: 'warning', danger: 'danger' },
    duration: {
      tooFast: 3, fast: 5,  medium: 10,  slow: 50
    },
    vPotion: { top: 'top', bottom: 'bottom' },
    hPotion: { center: 'center', end: 'end', left: 'left', right: 'right', start: 'start' }
  }
};







