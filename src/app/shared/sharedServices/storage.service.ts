import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }


  setStorage(token, value) {
    localStorage.setItem(token, value);
  }

  getStorage(token) {
    return localStorage.getItem(token);
  }

  clearStorage() {
    localStorage.clear();
  }

}
