import { StorageService } from './storage.service';
import { catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { SnackbarService } from './snackbar.service';
import { i18N } from '../constant';



@Injectable({
  providedIn: 'root'
})

export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector: Injector, public storage: StorageService, private router: Router, private snackbar: SnackbarService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const restrictedUrl = ['auth', 'register', 'reqresetpassword'];
    const reqUrl = req.url;
    const url = reqUrl.substring(reqUrl.lastIndexOf('/') + 1);
    if (!restrictedUrl.includes(url)) {
      const getToken = this.injector.get(StorageService);
      const token: string = getToken.getStorage('token');
      const tokenizedReq = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
      return next.handle(tokenizedReq).pipe(
        tap(event => {
          if (event instanceof HttpResponse
            && (event.body.status === i18N.status.InternalServerError || event.body.status === i18N.status.SessionExpired)) {
            localStorage.removeItem('token');
            localStorage.removeItem('id');
            this.snackbar.toaster(i18N.messages.alert, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
            this.router.navigate(['/login']);
          }
        },
          error => {
            if (error) {

              const router = this.injector.get(Router);
              router.navigate(['/login']);
            }
          }
        )
      );
    }
    return next.handle(req);
  }
}



