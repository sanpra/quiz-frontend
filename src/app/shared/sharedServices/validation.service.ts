import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  constructor() { }
  // function to set error messages
  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    const config = {

      required: 'This field is required',
      invalidEmailAddress: 'Invalid email address',
      invalidPassword: 'Invalid password',
      misMatchPassword: 'New password and confirm password does not match',
      emailTaken: 'User already registered'
    };
    return config[validatorName];
  }

  static emailValidator(control: AbstractControl) {
    if (control.value.length === 0 || control.value.match(/^[a-zA-Z._0-9]+@rishabhsoft.com$/)) {
      return null;
    } else {
      return { invalidEmailAddress: true };
    }
  }

  static passwordValidator(control: AbstractControl) {
    if (control.value.match(/^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9])(?=[^!@#$%^&*(),.?":{}|<>]*[!@#$%^&*(),.?":{}|<>]).{8,}$/)) {
      return null;
    } else {
      return { invalidPassword: true };
    }
  }

  static confirmPasswordValidator(control: AbstractControl) {
    const password: string = control.get('password').value; // get password from our password form control
    const confirmPassword: string = control.get('confirmPassword').value; // get password from our confirmPassword form control
    if (password !== confirmPassword) {
      control.get('confirmPassword').setErrors({ misMatchPassword: true });
    }
    return null;
  }

  static registerUserValidator(control: AbstractControl) {
    const email: string = control.get('email').value; // get email from our email form control
    // const confirmPassword: string = control.get('cpassword').value; // get password from our confirmPassword form control
    if (email === email) {
      control.get('email').setErrors({ emailTaken: true });
    }
    return null;
  }

}
