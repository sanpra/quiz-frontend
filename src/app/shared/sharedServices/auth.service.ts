import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router, private storage: StorageService) { }

  isLoggednIn() {
    return this.storage.getStorage('token');
  }

}
