import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackBar: MatSnackBar) { }

  public toaster(message: string, action: string, panel: string, duration: number = 10, vPotion: any = 'top', hPotion: any= 'end') {
    this.snackBar.open(message, action, {
      panelClass: panel,
      duration: duration * 1000,
      verticalPosition: vPotion,
      horizontalPosition: hPotion
    });
  }

}
