import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public falg=true;
  isLoading = new Subject<boolean>();
  show() {
    setTimeout(() => {
      if(this.falg){
        this.isLoading.next(true);
      }
    }, 3000);
  }
  hide() {
      this.falg=false;
      this.isLoading.next(false);
  }
}






