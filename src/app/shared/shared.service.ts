import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }

  alpha(findNum) {
    return String.fromCharCode(97 + findNum);
  }
}

