import { Register } from './../shared/interface';
import { PasswordStrengthValidator } from './../shared/sharedComponent/password-strength.validator';
import { SnackbarService } from './../shared/sharedServices/snackbar.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, NgForm } from '@angular/forms';
import { ValidationService } from '../shared/sharedServices/validation.service';
import { RegisterService } from './register.service';
import { Router } from '@angular/router';
import { i18N } from '../shared/constant';
import { MustMatch } from '../shared/sharedServices/must-match.validator';
import { User } from './../shared/model';
import { LoaderService } from '../shared/loader/loader.service';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public i18N = i18N;
  public registerForm: FormGroup;
  public response: any = {};
  public err: any;
  public showError = false;
  public showResponse = false;
  public user = new User();
  constructor(public fb: FormBuilder, public VS: ValidationService,
              public registerSvc: RegisterService, public route: Router,
              private snackBar: SnackbarService, private loaderService: LoaderService) { }
  public hide = true;

  ngOnInit() {
    this.buildForm();

  }

  private buildForm() {
    this.registerForm = this.fb.group({

      firstname: new FormControl('', [Validators.required]),
      lastname: new FormControl(''),
      email: new FormControl('', [Validators.required, Validators.pattern(this.i18N.regex.email)]),
      password: new FormControl('', [Validators.required, PasswordStrengthValidator, Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required]),
    },
      {
        validator: MustMatch('password', 'confirmPassword')
      });
  }

  // convenience getter for easy access to form fields
  get form() { return this.registerForm.controls; }

  public register(data: NgForm) {
    this.loaderService.show();
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      const newUser: Register = data.value;
      this.registerSvc.setData(newUser).subscribe(
        res => {
          if (res.status === i18N.status.Ok) {
            const msg = res.message; this.showResponse = true;
            this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.success, i18N.toaster.duration.tooFast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
            this.route.navigate(['login']);
          } else if (res.status === i18N.status.BadReqquest) {
            const msg = res.message;
            this.showResponse = true;
            this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.tooFast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
          } else {
            const msg = res.message;
            this.showResponse = true;
            this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.warning, i18N.toaster.duration.tooFast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
          }
          this.loaderService.hide();
        },
      );
    }

    this.registerForm.markAsPristine();
    this.registerForm.markAsUntouched();
  }

  enter() {
    this.hide = false;
  }
  leave() {
    this.hide = true;
  }
}
