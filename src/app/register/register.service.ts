import { Injectable } from '@angular/core';
import { Register } from '../shared/interface';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  setData(userRegister: Register): Observable<Register> {
    return this.http.post<Register>(`${environment.BASE_URL}/user/register`, userRegister);
  }
}
