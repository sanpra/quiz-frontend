import { ForgotPasswordService } from './forgot-password.service';
import { i18N } from './../shared/constant';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from '../shared/loader/loader.service';
import { SnackbarService } from '../shared/sharedServices/snackbar.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public forgotForm: FormGroup;
  public i18N = i18N;
  public showResponse = false;
  public showError = false;
  public user: any = {};

  constructor(private fb: FormBuilder, private forgotService: ForgotPasswordService, private loaderService: LoaderService,
              private snackBar: SnackbarService) { }

  ngOnInit() {
    this.formData();
  }


  formData() {
    this.forgotForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.pattern(this.i18N.regex.email)]),
    });
  }

  // convenience getter for easy access to form fields
  get form() { return this.forgotForm.controls; }

  forgotPassword() {
    this.loaderService.show();
    const user = {
      email: this.forgotForm.get('email').value
    };
    console.log(user.email);
    this.forgotService.sendEmail(user).subscribe(
      res => {
        const msg = res.message;
        this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.success, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
        this.loaderService.hide();
      }
    );
  }
}
