import { Login } from './../shared/interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(private http: HttpClient) { }

  sendEmail(user): Observable<Login> {
    return this.http.post<Login>(`${environment.BASE_URL}/auth/reqresetpassword`, user);
  }
}
