import { NgModule } from '@angular/core';

import {
  MatBadgeModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
  MatInputModule, MatBottomSheetModule, MatRadioModule, MatSelectModule, MatSliderModule,
  MatSlideToggleModule, MatMenuModule, MatSidenavModule,
  MatToolbarModule, MatCardModule, MatDividerModule, MatExpansionModule,
  MatGridListModule, MatListModule, MatStepperModule, MatTableModule, MatTreeModule,
  MatTabsModule, MatButtonModule, MatButtonToggleModule,
  MatChipsModule, MatIconModule, MatProgressSpinnerModule,
  MatProgressBarModule, MatRippleModule, MatDialogModule,
   MatSnackBarModule, MatTooltipModule, MatPaginatorModule, MatSortModule, MatAutocompleteModule
} from '@angular/material'




const material = [
  MatAutocompleteModule,
  MatBadgeModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatBottomSheetModule,
  MatRadioModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatCardModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatListModule,
  MatStepperModule,
  MatTableModule,
  MatTreeModule,
  MatTabsModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatRippleModule,
  MatDialogModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatSortModule,

]

@NgModule({

  imports: [material],
  exports: [material]
})
export class MaterialModule { }
