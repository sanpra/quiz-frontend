import { TokenInterceptorService } from './shared/sharedServices/token-interceptor.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { LoginComponent } from './login/login.component';
import { PagesModule } from './pages/pages.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { ValidationMessageComponent } from './shared/sharedComponent/validation-message.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './shared/guard/auth.guard';
import { AuthService } from './shared/sharedServices/auth.service';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { LoaderComponent } from './shared/loader/loader.component';
import { LoaderService } from './shared/loader/loader.service';
import { ResultPopupComponent } from './pages/result-popup/result-popup.component';

@NgModule({
  declarations: [

    AppComponent, LoginComponent, RegisterComponent, ValidationMessageComponent, ResetPasswordComponent,
    ForgotPasswordComponent, LoaderComponent],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    PagesModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ], entryComponents: [
    ValidationMessageComponent, LoaderComponent, ResultPopupComponent
  ],
  providers: [AuthGuard, AuthService, LoaderService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
