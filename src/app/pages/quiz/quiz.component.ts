import { SnackbarService } from './../../shared/sharedServices/snackbar.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PagesService } from './../pages.service';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { QuizPopupComponent } from '../quiz-popup/quiz-popup.component';
import { i18N } from '../../shared/constant';
import { LoaderService } from 'src/app/shared/loader/loader.service';
import { debounce } from 'lodash';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {

  public quizes: any = [];
  public i18N = i18N;
  public dataSource: any;
  public quizData: any;
  public totalrecords: number;
  public currentPage = 0;
  public pageSize = 10;
  public pageSizeOptions = [5, 10, 20, 50, 100];
  public title = ' ';
  public searchForm: FormGroup;
  public results = '';
  public hideOptions = false;
  public paginator: MatPaginator;
  public duration: number;
  public noRecord = false;

  constructor(private pageService: PagesService,
    public dialog: MatDialog,
    private changeDetectorRef: ChangeDetectorRef,
    private loaderService: LoaderService,
    private formBuilder: FormBuilder,
    private router: Router,
    private snackBar: SnackbarService) { }

  fillter = debounce(() => {
    this.title = this.searchForm.get('searchInput').value;
    if (this.title.length >= 3) {
      const pageNumber = 0;
      this.currentPage = 0;
      this.cards(pageNumber, this.pageSize, this.title);
      this.hideOptions = true;
    } else {
      const pageNumber = 0;
      this.cards(pageNumber, this.pageSize, ' ');
      this.hideOptions = false;
    }
  }, environment.debounceTime);


  ngOnInit() {
    this.getquiz();
    this.searchForm = this.formBuilder.group({
      searchInput: ''
    });
  }

  get form(): any { return this.searchForm.get('searchInput'); }
  clear() {
    this.form.reset();
    this.title =  '';
    this.getquiz();
  }
  pageSizeChange($event) {
    this.loaderService.show();
    this.currentPage = $event.pageIndex;
    this.pageSize = $event.pageSize;
    const pageNumber = this.currentPage + 1;
    this.cards(pageNumber, this.pageSize, this.title);
  }


  getquiz() {
    this.loaderService.show();
    this.cards(this.currentPage, this.pageSize, this.title);
  }

  cards(pageNumber, pageSize, title) {
    this.pageService.getquiz(pageNumber, pageSize, title).subscribe(
      res => {
        if (res.data) {
          this.quizes = res.data.quiz;
          this.results = res.data.result;
          this.quizes = this.quizes.map((quiz) => {
            const dayLeft = Math.abs(moment(new Date()).diff(moment(quiz.endDate), 'days'));
            if (dayLeft >= 7) {
              const weekLeft = Math.abs(
                moment(new Date()).diff(moment(quiz.endDate), 'weeks')
              );
              if (weekLeft > 1) {
                quiz.endDate = `${weekLeft} ${i18N.time.weeks}`;
              } else {
                quiz.endDate = `${weekLeft} ${i18N.time.week}`;
              }
            } else {
              if (dayLeft > 1) {
                quiz.endDate = `${dayLeft} ${i18N.time.days}`;
              } else {
                quiz.endDate = `${dayLeft} ${i18N.time.day}`;
              }
            }
            quiz.createdAt = quiz.createdAt.slice(0, 10);
            return quiz;
          });
          this.totalrecords = res.data.totalRecords;
          if (this.totalrecords === 0) {
            this.noRecord = true;
          } else {
            this.noRecord = false;
          }
          this.changeDetectorRef.detectChanges();
          this.dataSource = new MatTableDataSource<any>(this.quizes);
          this.dataSource.paginator = this.paginator;
          this.quizData = this.dataSource.connect();
        }
        this.loaderService.hide();
      }
    );
  }

  isChecked(status, results, quiz) {
    if (status === i18N.quizStatus.created && !results) {
      this.popup(quiz);
    }
    if (status === i18N.quizStatus.created && results) {
      this.snackBar.toaster(i18N.messages.appeard + ' ' + results, i18N.toaster.action, i18N.toaster.panel.warning,
      i18N.toaster.duration.fast, i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
    }
    if (status === i18N.quizStatus.expired && results) {
      this.router.navigate(['pages/result', quiz._id]);
    }
    if (status === i18N.quizStatus.expired && !results) {
      this.snackBar.toaster(i18N.messages.notAttempted, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
      this.router.navigate(['pages/result', quiz._id]);
    }
  }




  popup(quiz) {
    const data = quiz;
    this.dialog.open(QuizPopupComponent, {
      panelClass: 'dialog',
      data: { title: data.title, id: data._id, description: data.description, duration: data.duration, isdata: true }
    });
  }

  OnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }
}


