import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './../material/material.module';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { QuizComponent } from './quiz/quiz.component';
import { ProfileComponent } from './profile/profile.component';
import { ResultComponent } from './result/result.component';
import { QuizPopupComponent } from './quiz-popup/quiz-popup.component';
import { QuizQuestionsComponent } from './quiz-questions/quiz-questions.component';
import { ResultPopupComponent } from './result-popup/result-popup.component';
import { CountdownModule } from 'ngx-countdown';



@NgModule({
  declarations: [PagesComponent, QuizComponent, ProfileComponent,
    ResultComponent, QuizPopupComponent, QuizQuestionsComponent, ResultPopupComponent],
  imports: [CommonModule, SharedModule, MaterialModule, RouterModule, FormsModule, ReactiveFormsModule, CountdownModule],
  entryComponents: [QuizPopupComponent]
})
export class PagesModule { }
