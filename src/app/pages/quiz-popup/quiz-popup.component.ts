import { QuizComponent } from './../quiz/quiz.component';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { i18N } from '../../shared/constant';
import { DialogData } from 'src/app/shared/interface';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-quiz-popup',
  templateUrl: './quiz-popup.component.html',
  styleUrls: ['./quiz-popup.component.scss']
})
export class QuizPopupComponent implements OnInit {

  public i18N = i18N;

  constructor(public dialogRef: MatDialogRef<QuizComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private route: ActivatedRoute, private router: Router) { }
  ngOnInit(
  ) { }

  close() {
    this.dialogRef.close();
  }

  startQuiz(id) {
    this.router.navigate(['/pages/quiz', id]);
    this.dialogRef.close();
  }
  closeModel() {
    this.router.navigate(['/pages']);
    this.dialogRef.close();
  }
}
