import { Component, OnInit, Inject } from '@angular/core';
import { i18N } from '../../shared/constant';
import { QuizQuestionsComponent } from '../quiz-questions/quiz-questions.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { ResultData } from 'src/app/shared/interface';

@Component({
  selector: 'app-result-popup',
  templateUrl: './result-popup.component.html',
  styleUrls: ['./result-popup.component.scss']
})
export class ResultPopupComponent implements OnInit {

  public i18N = i18N;

  constructor(public dialogRef: MatDialogRef<QuizQuestionsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ResultData,
              private router: Router) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
    this.router.navigate(['/pages']);
  }
  submit(submit: boolean) {
    this.dialogRef.close(submit);
  }
}
