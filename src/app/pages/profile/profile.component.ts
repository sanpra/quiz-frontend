import { Component, OnInit } from '@angular/core';
import { i18N } from '../../shared/constant';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public i18N = i18N;

  constructor() { }

  ngOnInit() {
  }

}
