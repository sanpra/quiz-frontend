import { User } from './../shared/model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { StorageService } from './../shared/sharedServices/storage.service';
import { Injectable } from '@angular/core';
import { Login } from '../shared/interface';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Quiz } from '../shared/interface';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  constructor(private storage: StorageService,
    private router: Router,
    private http: HttpClient) { }

  logout(user): Observable<Login> {
    const id = this.storage.getStorage('id');
    return this.http.put<Login>(`${environment.BASE_URL}/user/${id}`, user);
  }


  getquiz(currentPage, recordPerPage, title): Observable<Quiz> {
    return this.http.get<Quiz>(`${environment.BASE_URL}/quiz?pageNumber=${currentPage}&recordsPerPage=${recordPerPage}&title=${title}`);
  }

  getQuestions(quizId): Observable<any> {
    return this.http.get<any>(`${environment.BASE_URL}/quiz/questions/${quizId}`);
  }

  user(): Observable<User> {
    return this.http.get<User>(`${environment.BASE_URL}/user`);
  }

  getResult(quizId): Observable<any> {
    return this.http.get<any>(`${environment.BASE_URL}/result/${quizId}`);
  }
}
