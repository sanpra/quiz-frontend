import { SnackbarService } from './../shared/sharedServices/snackbar.service';
import { User } from './../shared/model';
import { i18N } from './../shared/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../shared/sharedServices/auth.service';
import { Component, OnInit } from '@angular/core';
import { PagesService } from './pages.service';
import { StorageService } from '../shared/sharedServices/storage.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  public i18N = i18N;
  public user: any = {};
  public url: any;
  public pointer = false;
  public menuFlag = false;
  constructor(private auth: AuthService, private router: Router,
              private pageService: PagesService,
              private storage: StorageService,
              private snackbar: SnackbarService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (!this.auth.isLoggednIn) {
      this.router.navigate(['/login']);
    }
    this.users();
    this.route.url.subscribe(() => {
      this.url = (this.route.snapshot.firstChild.routeConfig.path);
      if (this.url === 'quiz/:id') {
        this.pointer = true;
        this.menuFlag = true;
      } else {
        this.pointer = false;
        this.menuFlag = false;
      }
    });
  }

  logout() {
    const reqUrl = window.location.href;
    const url = reqUrl.indexOf('quiz') > 0;
    if (!url) {
      this.pageService.logout(User).subscribe(
        res => {
          const msg = res.message;
          this.storage.clearStorage(),
            this.snackbar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.success, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
          this.router.navigate(['/login']);
        }
      );
    }
  }

  users() {
    this.pageService.user().subscribe(
      res => {
        this.user = res.data;
      }
    );
  }

}
