import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PagesService } from '../pages.service';
import { i18N } from '../../shared/constant';
import { ResultReport } from '../../shared/interface';
import {SharedService} from '../../shared/shared.service';
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  public id: any;
  public i18N = i18N;
  public data: ResultReport;
  public title: ResultReport;
  constructor(private route: ActivatedRoute, private pageService: PagesService, public share: SharedService) {}

  ngOnInit() {
    this.result();
  }

  result() {
    this.id = this.route.snapshot.params.id;
    this.pageService.getResult(this.id).subscribe(
      res => {
        this.data = res.data.result;
        this.title = res.data.quizTitle;
      }
    );
  }
}
