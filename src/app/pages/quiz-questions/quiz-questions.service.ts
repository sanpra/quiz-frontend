import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuizQuestionsService {

  constructor(private http: HttpClient) { }


  submit(userResponse: any): Observable <any> {
    return this.http.post<any>(`${environment.BASE_URL}/result/submit`, userResponse);
  }
}
