import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PagesService } from '../pages.service';
import { i18N } from '../../shared/constant';
import { QuizQuestionsService } from './quiz-questions.service';
import { ResultPopupComponent } from '../result-popup/result-popup.component';
import { MatDialog } from '@angular/material';
import { LoaderService } from 'src/app/shared/loader/loader.service';
import { HostListener } from '@angular/core';
import { CountdownComponent, CountdownConfig } from 'ngx-countdown';
import { SnackbarService } from 'src/app/shared/sharedServices/snackbar.service';
import { SharedService } from '../../shared/shared.service';
import { QuizPopupComponent } from '../quiz-popup/quiz-popup.component';

@Component({
  selector: 'app-quiz-questions',
  templateUrl: './quiz-questions.component.html',
  styleUrls: ['./quiz-questions.component.scss']
})


export class QuizQuestionsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private pageService: PagesService,
              public quizQuestionSvc: QuizQuestionsService, public routes: Router,
              public dialog: MatDialog, public loaderSvc: LoaderService,
              private snackBar: SnackbarService,
              public share: SharedService) { }

  public id: any;
  public data: any = {};
  public question;
  public i = 0;
  public quizAnswers: any = [];
  public questionId: any;
  public answer: any;
  public selectedOption: string;
  public i18N = i18N;
  public userResponse: any = {};
  public showResponse = false;
  public saveFlag = false;
  public submitFlag = false;
  public duration: number;
  public timeTaken: number;
  public showMessage = false;
  public backgroundFlag = true;
  public leftTime: number;
  public format: string;
  public notify: any;
  public demand: boolean;
  public config: CountdownConfig = {};
  public progressBarValue = 0;
  public noQuestion: number;
  public noAnswers = 0;
  @ViewChild('countdown', { static: false }) counter: CountdownComponent;

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    if (confirm(i18N.messages.browserBackAlert)) {
      this.submit();
    } else {
      event.preventDefault();
      return false;
    }
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    event.preventDefault();
    return false;
  }

  ngOnInit() {
    this.questions();
  }
  next() {
    let overRideFlag = false;
    this.questionId = this.question._id;
    this.answer = this.selectedOption;

    if (this.answer) {

      this.quizAnswers.find((qA) => {
        if (qA.questionId === this.questionId) {
          qA.answer = this.answer;
          overRideFlag = true;
        }
      });

      if (!overRideFlag) {

        const temp = { questionId: this.questionId, answer: this.answer };
        this.quizAnswers.push(temp);
        overRideFlag = false;
      }
    }
    if (this.i < (this.data.questions.length - 1)) {
      this.i++;
      this.que();
      this.quizAnswers.find((qA) => {
        if (qA.questionId === this.question._id) {
          this.selectedOption = qA.answer;
        }
      });
    } else {
      this.saveFlag = true;
    }
    this.progressBarValue = Number((this.quizAnswers.length * 100) / this.data.questions.length);
    this.noAnswers = this.quizAnswers.length;

  }

  previous() {
    if (this.i > 0) {
      this.i--;
      this.que();
      this.quizAnswers.find((qA) => {
        if (qA.questionId === this.question._id) {
          this.selectedOption = qA.answer;
        }
      });
    }
  }

  que() {
    if ( this.data === false) {
        this.submitFlag = true;
        this.routes.navigate(['/pages']);
        this.snackBar.toaster(i18N.messages.reloadError, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
    } else {
      this.question = this.data.questions[this.i];
      this.selectedOption = null;
    }
  }

  questions() {
    this.loaderSvc.show();
    this.id = this.route.snapshot.params.id;
    this.pageService.getQuestions(this.id).subscribe(
      res => {
        if (res.status === i18N.status.BadReqquest) {
          this.loaderSvc.hide();
          this.popupque(res.message);
          this.backgroundFlag = false;
        } else if (res.status === i18N.status.InternalServerError || res.status === i18N.status.SessionExpired) {
          this.loaderSvc.hide();
          this.submitFlag = true;
        } else {
          this.data = res.data;
          this.loaderSvc.hide();
          this.startQuiz();
          this.duration = this.data.duration * 60;
          this.que();
          this.config = {
            leftTime: this.duration,
            format: 'mm  :  ss',
            notify: [60],
            demand: false
          };
        }
      }
    );

  }

  submit() {
    this.popupConfirm();
  }


  submitQuiz() {
    this.timeCal();
    this.submitFlag = true;
    this.userResponse.quizId = this.id;
    this.userResponse.timeTaken = this.timeTaken;
    this.userResponse.quizAnswers = this.quizAnswers;
    this.quizQuestionSvc.submit(this.userResponse).subscribe(
      res => {
        this.loaderSvc.hide();
        if (res.status === i18N.status.Ok) {
          this.popup(res.data);
        } else {
          const msg = res.message;
          this.showResponse = true;
          this.routes.navigate(['/pages']);
        }
      });
  }

  startQuiz() {
    this.userResponse.quizId = this.id;
    this.quizQuestionSvc.submit(this.userResponse).subscribe(
      res => { this.loaderSvc.hide();
               if (res.status !== 200) {
        this.submitFlag = true;
        this.routes.navigate(['/login']);
        const msg = i18N.messages.serverError;
        this.showMessage = true;
        this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
      } }
      );
}

  popup(result) {
    const data = result;
    this.backgroundFlag = false;
    const dialogRef = this.dialog.open(ResultPopupComponent, {
      width: '300px',
      height: '350px',
      data: {
        result: data.result, userScore: data.userScore,
        userPercentage: data.userPercentage, isforResult: true
      }
    });
  }

  popupConfirm() {
    const dialogRef = this.dialog.open(ResultPopupComponent, {
      width: '400px',
      height: '200px',
      data: {
        isforResult: false
      }
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.loaderSvc.show();
        if (!this.saveFlag) {
          this.next();
          this.submitQuiz();
        } else {
          this.saveFlag = false;
          this.submitQuiz();
        }
      }
    });
  }


  canExit(): boolean {
    if (this.submitFlag) {
      this.submitFlag = false;
      return true;
    } else {
      return false;
    }
  }

  timeCal() {
    this.counter.stop();
    const timeTakenM: number = Math.floor((this.duration - ((this.counter.i.value) / 1000)) / 60);
    const timeTakenS: number = (this.duration - ((this.counter.i.value) / 1000)) % 60;
    this.timeTaken = Number(timeTakenM + '.' + timeTakenS);
  }
  timesEvents(event) {
    if (event.action === 'notify') {
      const msg = i18N.messages.warningAlert;
      this.showMessage = true;
      this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.medium,
        i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
    } else if (event.action === 'done') {
      if (!this.saveFlag) {
        this.next();
        this.submitQuiz();
      } else {
        this.saveFlag = false;
        this.submitQuiz();
      }
      const msg = i18N.messages.timeout;
      this.showMessage = true;
      this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.medium,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
    }
  }

  popupque(message) {
    const dialogRef = this.dialog.open(QuizPopupComponent, {
      width: '400px',
      height: '200px',
      data: { msg: message, isdata: false }
    });
    dialogRef.afterClosed().subscribe(data => {
      this.submitFlag = true;
      this.routes.navigate(['/pages']);
    });
  }
}
