import { AuthService } from './../shared/sharedServices/auth.service';
import { StorageService } from './../shared/sharedServices/storage.service';
import { Router } from '@angular/router';
import { SnackbarService } from './../shared/sharedServices/snackbar.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormGroupDirective, NgForm } from '@angular/forms';
import { i18N } from '../shared/constant';
import { LoginService } from './login.service';
import { LoaderService } from '../shared/loader/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public i18N = i18N;
  public err: any;
  public showResponse = false;
  public showError = false;
  public hide = true;
  public loginForm: FormGroup;
  public user: any = {};
  public visibilityOff = 'visibility_off';
  public visibility = 'visibility';
  constructor(
    private route: Router,
    private snackBar: SnackbarService,
    private fb: FormBuilder,
    private loginService: LoginService,
    private storage: StorageService,
    private Auth: AuthService,
    private loaderService: LoaderService
  ) { }


  ngOnInit() {
    this.formData();
    if (this.Auth.isLoggednIn) {
      this.route.navigate(['pages']);
    }
  }

  formData() {
    this.loginForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.pattern(this.i18N.regex.email)]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });

  }
  // convenience getter for easy access to form fields
  get form() { return this.loginForm.controls; }


  login() {
    this.loaderService.show();
    this.user.email = this.loginForm.get('email').value;
    this.user.password = this.loginForm.get('password').value;

    this.loginService.login(this.user).subscribe(
      res => {
        if (res.status === i18N.status.Ok) {
          const response = res.message; this.showResponse = true;
          const token = res.data.accessToken;
          const id = res.data._id;
          this.storage.setStorage('token', token);
          this.storage.setStorage('id', id);
          this.snackBar.toaster(response, i18N.toaster.action, i18N.toaster.panel.success, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
          this.route.navigate(['pages']);
        } else if (res.status === i18N.status.Unauthorized) {
          const response = res.message; this.showResponse = true;
          this.snackBar.toaster(response, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
        } else {
          const response = res.message; this.showResponse = true;
          this.snackBar.toaster(response, i18N.toaster.action, i18N.toaster.panel.warning, i18N.toaster.duration.fast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
        }
        this.loaderService.hide();
      }
    );

    this.loginForm.markAsPristine();
    this.loginForm.markAsUntouched();
  }
  enter() {
    this.hide = false;
  }
  leave() {
    this.hide = true;
  }

}
