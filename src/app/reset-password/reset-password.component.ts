import { StorageService } from '../shared/sharedServices/storage.service';
import { ResetPasswordService } from './resetpassword.service';
import { i18N } from './../shared/constant';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PasswordStrengthValidator } from '../shared/sharedComponent/password-strength.validator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MustMatch } from '../shared/sharedServices/must-match.validator';
import { SnackbarService } from '../shared/sharedServices/snackbar.service';
import { LoaderService } from '../shared/loader/loader.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})

export class ResetPasswordComponent implements OnInit {
  public i18N = i18N;
  public resetForm: FormGroup;
  public showResponse = false;
  public showError = false;
  public token: string;
  public card = false;
  public errormsg: string;
  public hide = true;
  public visibilityOff = 'visibility_off';
  public visibility = 'visibility';
  constructor(private fb: FormBuilder, private route: ActivatedRoute,
              private resetService: ResetPasswordService,
              private snackBar: SnackbarService,
              private router: Router,
              public storage: StorageService, private loaderService: LoaderService) { }

  ngOnInit() {
    this.formData();
    this.verifyToken();
  }

  formData() {
    this.resetForm = this.fb.group({
      password: new FormControl('', [Validators.required, PasswordStrengthValidator, Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required]),
    },
      {
        validator: MustMatch('password', 'confirmPassword')
      });

  }

  get form() { return this.resetForm.controls; }

  verifyToken() {
    this.loaderService.show();
    this.route.params.forEach((params: Params) => {
      this.token = params.token;
    });
    const token = this.token;
    this.storage.setStorage('token', token);
    this.resetService.getToken(this.token).subscribe(
      res => {
        if (res.status === i18N.status.Ok) {
          this.card = true;
        }
        if (res.status === i18N.status.InternalServerError) {
          const msg = res.message;
          this.errormsg = msg;
          this.storage.clearStorage();
          this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.warning, i18N.toaster.duration.tooFast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
          this.router.navigate(['/login']);
          this.snackBar.toaster(i18N.messages.alert, i18N.toaster.action, i18N.toaster.panel.danger, i18N.toaster.duration.tooFast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
        }
        this.loaderService.hide();
      },
      err => {
        console.log(err);
        this.loaderService.hide();
      }
    );

  }

  changePassword() {
    this.loaderService.show();
    const pass = {
      password: this.resetForm.get('password').value,
      confirmPassword: this.resetForm.get('confirmPassword').value
    };
    this.resetService.changePassword(pass).subscribe(
      res => {
        if (res.status === i18N.status.Ok) {
          const msg = res.message;
          this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.success, i18N.toaster.duration.tooFast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
          this.storage.clearStorage();
          this.router.navigate(['/login']);
        }
        if (res.status === i18N.status.InternalServerError) {
          const msg = res.message;
          this.storage.clearStorage();
          this.router.navigate(['/login']);
          this.snackBar.toaster(msg, i18N.toaster.action, i18N.toaster.panel.warning, i18N.toaster.duration.tooFast,
              i18N.toaster.vPotion.top, i18N.toaster.hPotion.end);
        }
        this.loaderService.hide();
      },
      err => {
        console.log(err);
        this.loaderService.hide();
      }
    );
    this.resetForm.markAsPristine();
    this.resetForm.markAsUntouched();
  }
  enter() {
    this.hide = false;
  }
  leave() {
    this.hide = true;
  }
}

