import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})

export class ResetPasswordService {

    constructor(private http: HttpClient) { }

    getToken(token): Observable<any> {
        return this.http.get<any>(`${environment.BASE_URL}/auth/validatingtoken/` + token);
    }

    changePassword(password): Observable<any> {
        return this.http.post<any>(`${environment.BASE_URL}/user/newpassword`, password);
     }
}
